//
//  AGSliderWithProgressBar.m
//  AGSliderWithProgress
//
//  Created by Håvard Fossli on 30.01.13.
//  Copyright 2013 Håvard Fossli. All rights reserved.
//

#import "AGSliderWithProgressBar.h"
#import <QuartzCore/QuartzCore.h>


static CGFloat progressForValue(CGFloat startValue, CGFloat endValue, CGFloat value)
{
    CGFloat diff = value - startValue;
    CGFloat scope = endValue - startValue;
    CGFloat progress;
    
    if(diff != 0.0)
    {
        progress = diff / scope;
    }
    else
    {
        progress = 0.0f;
    }
    
    return progress;
}

static CGFloat valueInterpolate(CGFloat startValue, CGFloat endValue, CGFloat progress)
{
    CGFloat diff = endValue - startValue;
    return startValue + (diff * progress);
}

static UIViewAutoresizing UIViewAutoresizingCentered()
{
    return UIViewAutoresizingFlexibleLeftMargin
    | UIViewAutoresizingFlexibleRightMargin
    | UIViewAutoresizingFlexibleTopMargin
    | UIViewAutoresizingFlexibleBottomMargin;
}

static CGRect CGRectFromHeightCenteredInRect(CGFloat height, CGRect rect)
{
    return CGRectMake(0,
                      (rect.size.height / 2.0) - (height / 2.0),
                      rect.size.width,
                      height);
}

static CGRect CGRectFromSizeSenteredInRect(CGSize size, CGRect rect)
{
    return CGRectMake((rect.size.width / 2.0) - (size.width / 2.0),
                      (rect.size.height / 2.0) - (size.height / 2.0),
                      size.width,
                      size.height);
}



@interface AGSliderWithProgressBar ()

@property (nonatomic, strong) UIButton *trackThumb;
@property (nonatomic, strong) UIImageView *minimumTrackView;
@property (nonatomic, strong) UIImageView *maximumTrackView;
@property (nonatomic, strong) UIImageView *maximumProgressView;

@end


@implementation AGSliderWithProgressBar

#pragma mark - Construct and destruct

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {
        [self setup];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        [self setup];
    }
    return self;    
}

- (void)setMinimumValue:(CGFloat)minimumValue
{
    if(_minimumValue != minimumValue)
    {
        _minimumValue = minimumValue;
        [self setNeedsLayout];
    }
}

- (void)setMaximumValue:(CGFloat)maximumValue
{
    if(_maximumValue != maximumValue)
    {
        _maximumValue = maximumValue;
        [self setNeedsLayout];
    }
}

- (void)setProgressValue:(CGFloat)progressValue
{
    if(_progressValue != progressValue)
    {
        _progressValue = progressValue;
        [self setNeedsLayout];
    }
}

- (void)setValue:(CGFloat)value
{
    BOOL isNan = value != value;
    CGFloat valueConstrained = isNan ? 0.0 : MIN(self.maximumValue, MAX(self.minimumValue, value));
    if(valueConstrained != _value)
    {
        _value = valueConstrained;
        if(_flags.isTracking)
        {
            [self sendActionsForControlEvents:UIControlEventValueChanged];
        }
        [self setNeedsLayout];
    }
}

- (void)setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];
    
    self.trackThumb.highlighted = highlighted;
}

- (void)setEnabled:(BOOL)enabled
{
    [super setEnabled:enabled];
    
    self.trackThumb.enabled = enabled;
}

- (void)setSelected:(BOOL)selected
{
    [super setSelected:selected];
    
    self.trackThumb.selected = selected;
}

- (void)handleTouch:(UITouch *)touch
{
    CGRect trackRect = [self trackRectForBounds:self.bounds];
    CGPoint currentTouchPoint = [touch locationInView:self];
    CGFloat valueAsProgress = progressForValue(trackRect.origin.x, CGRectGetMaxX(trackRect), currentTouchPoint.x);
    CGFloat newValue = valueInterpolate(self.minimumValue, self.maximumValue, valueAsProgress);
    
    self.value = newValue;
}

- (BOOL)beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
    if(!_flags.isTracking)
    {
        _flags.isTracking = YES;
        [self handleTouch:touch];
    }
    return [super beginTrackingWithTouch:touch withEvent:event];
}

- (BOOL)continueTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
    if (_flags.isTracking && self.enabled)
    {
        [self handleTouch:touch];
    }
    return _flags.isTracking;
}

- (void)cancelTrackingWithEvent:(UIEvent *)event
{
    [super cancelTrackingWithEvent:event];
}

- (void)endTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
    _flags.isTracking = NO;
    
    [self handleTouch:touch];
    
    [self setNeedsLayout]; 
    
    [super endTrackingWithTouch:touch withEvent:event];
}

- (CGRect)trackRectForBounds:(CGRect)bounds
{
    CGSize trackButtonSize = self.trackThumb.frame.size;
    CGRect rect = CGRectInset(bounds, trackButtonSize.width / 2.0, 0);
    return rect;
}

- (CGRect)trackThumbRectForBounds:(CGRect)bounds 
{
    return CGRectInset([self trackRectForBounds:bounds], 5, 0);
}

- (void)layoutSubviews
{
    if(self.bounds.size.width < 40.0)
    {
        return;
    }
    
    CGRect trackRect = [self trackRectForBounds:self.bounds];
    CGRect trackThumbRect = [self trackThumbRectForBounds:self.bounds];
    CGFloat trackRectMinX = CGRectGetMinX(trackRect);
    CGFloat trackRectMaxX = CGRectGetMaxX(trackRect);
    CGFloat trackRectHeight = trackRect.size.height;
    CGFloat progress = progressForValue(self.minimumValue, self.maximumValue, self.progressValue);
    CGFloat trackProgress = progressForValue(self.minimumValue, self.maximumValue, self.value);
    CGFloat thumbPositionMidX = valueInterpolate(CGRectGetMinX(trackThumbRect), CGRectGetMaxX(trackThumbRect), trackProgress);
    CGFloat progressMaxX = valueInterpolate(CGRectGetMinX(trackRect), CGRectGetMaxX(trackRect), progress);
    
    {
        CGRect minimumTrackRect = self.minimumTrackView.frame;
        minimumTrackRect.origin.x = trackRect.origin.x;
        minimumTrackRect.origin.y = (trackRectHeight / 2.0) - (minimumTrackRect.size.height / 2.0);
        minimumTrackRect.size.width = thumbPositionMidX - trackRectMinX;
        self.minimumTrackView.frame = minimumTrackRect;
    }
    
    {
        CGRect maximumTrackRect = self.maximumTrackView.frame;
        maximumTrackRect.origin.x = thumbPositionMidX;
        maximumTrackRect.origin.y = (trackRectHeight / 2.0) - (maximumTrackRect.size.height / 2.0);
        maximumTrackRect.size.width = trackRectMaxX - thumbPositionMidX;
        self.maximumTrackView.frame = maximumTrackRect;
    }
    
    {
        CGRect maximumProgressRect = self.maximumProgressView.frame;
        CGFloat maximumProgressWidth = MAX(0, progressMaxX - thumbPositionMidX);
        maximumProgressRect.origin.x = thumbPositionMidX;
        maximumProgressRect.origin.y = (trackRectHeight / 2.0) - (maximumProgressRect.size.height / 2.0);
        maximumProgressRect.size.width = maximumProgressWidth;
        self.maximumProgressView.frame = maximumProgressRect;
    }
    
    {
        CGPoint trackThumbPosition = self.trackThumb.center;
        trackThumbPosition.x = thumbPositionMidX;
        //trackThumbPosition.y = 0;
        self.trackThumb.center = trackThumbPosition;
    }
}

static UIImage * transparentImageWithSize(CGSize size)
{
    UIGraphicsBeginImageContextWithOptions(size, NO, 0);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (void)setupMaximumTrackView
{
    UIImage *image = [UIImage imageNamed:@"mp_maximum_track.png"]; 
    UIImage *stretchableImage = [image stretchableImageWithLeftCapWidth:2 topCapHeight:image.size.height / 2.0];
    UIImageView *view = [[UIImageView alloc] initWithImage:stretchableImage];
    [self addSubview:view];
    self.maximumTrackView = view;
}

- (void)setupMinimumTrackView
{
    UIImage *image = [UIImage imageNamed:@"mp_minimum_track.png"]; 
    UIImage *stretchableImage = [image stretchableImageWithLeftCapWidth:image.size.width - 3.0 topCapHeight:image.size.height / 2.0];
    UIImageView *view = [[UIImageView alloc] initWithImage:stretchableImage];
    [self addSubview:view];
    self.minimumTrackView = view;
}

- (void)setupMaximumProgressView
{
    UIImage *image = [UIImage imageNamed:@"mp_maximum_progress.png"];
    UIImage *stretchableImage = [image stretchableImageWithLeftCapWidth:3 topCapHeight:image.size.height / 2.0];
    UIImageView *view = [[UIImageView alloc] initWithImage:stretchableImage];
    [self addSubview:view];
    self.maximumProgressView = view;
}

- (void)setupTrackThumb
{
    UIImage *trackThumbImage = [UIImage imageNamed:@"mp_tracker_thumb.png"];
    UIImage *trackThumbImageHighlighted = [UIImage imageNamed:@"mp_tracker_thumb_h.png"];
    
    self.trackThumb = [[UIButton alloc] initWithFrame:CGRectFromSizeSenteredInRect(trackThumbImage.size, self.bounds)];
    [self.trackThumb setImage:trackThumbImage forState:UIControlStateNormal];
    [self.trackThumb setImage:trackThumbImageHighlighted forState:UIControlStateHighlighted];
    self.trackThumb.userInteractionEnabled = NO;
    [self addSubview:self.trackThumb];
}

- (void)setup
{
    _minimumValue = 0.0;
    _maximumValue = 1.0;
    _progressValue = 0.0;
    _value = 0.0;
    
    self.backgroundColor = [UIColor clearColor];

    [self setupMaximumTrackView];
    [self setupMinimumTrackView];
    [self setupMaximumProgressView];
    [self setupTrackThumb];
    
    [self setNeedsLayout];
    
}

@end