//
//  AGMoviePlayerViewController.m
//  VG
//
//  Created by Håvard Fossli on 29.01.13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "AGMoviePlayerViewController.h"
#import "AGSliderWithProgressBar.h"
#import "AGMoviePlayerRouteView.h"
#import <AVFoundation/AVFoundation.h>

NSString * const AGMoviePlayerViewVisibleNotification = @"AGMoviePlayerViewVisibleNotification";
NSString * const AGMoviePlayerViewNotVisibleNotification = @"AGMoviePlayerViewNotVisibleNotification";

@interface AGMoviePlayerView : UIView

@end

@implementation AGMoviePlayerView

- (void)didMoveToWindow
{
    [super didMoveToWindow];
    if(self.window == nil)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:AGMoviePlayerViewNotVisibleNotification object:self];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:AGMoviePlayerViewVisibleNotification object:self];
    }
}

@end


@interface AGMoviePlayerViewController ()

@property (nonatomic, strong, readwrite) MPMoviePlayerController *moviePlayer;
@property (nonatomic, strong) IBOutlet NSURL *contentURL;
@property (nonatomic, strong) IBOutlet UILabel *labelTimeInfo;
@property (nonatomic, strong) IBOutlet AGSliderWithProgressBar *slider;
@property (nonatomic, strong) IBOutlet UIButton *buttonPlay;
@property (nonatomic, strong) IBOutlet UIButton *buttonPause;
@property (nonatomic, strong) IBOutlet UIButton *buttonToggleFullscreen;
@property (nonatomic, strong) IBOutlet UIView *volumeViewContainer;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) IBOutlet NSTimer *timer;
@property (nonatomic, strong) IBOutletCollection(UIView) NSArray *controlsContainers;
@property (nonatomic, strong) IBOutlet AGMoviePlayerRouteView *volumeView;

@end


@implementation AGMoviePlayerViewController

+ (void)initialize
{
    [self enableAudioInSilenceMode];
}

- (id)initWithContentURL:(NSURL *)contentURL
{
    self = [self initWithNibName:nil bundle:nil];
    if(self)
    {
        _flags.controlsIsVisible = TRUE;
        self.contentURL = contentURL;
    }
    return self;
}

- (void)viewDidLoad
{
    self.moviePlayer = [self createMoviePlayer];
    
    [self setupVolumeView];
    
    [self setupObservers];
    
    [self.view insertSubview:self.moviePlayer.view atIndex:0];
    [self.moviePlayer play];
    
    [self setupRecognizers];
    
    [self updateFullscreenFlagIfPossible];
    
    [self updateControlsAnimated:NO];
    
    [self.buttonToggleFullscreen setImage:[self.buttonToggleFullscreen imageForState:UIControlStateSelected] forState:UIControlStateSelected | UIControlStateHighlighted];
    [self updateVolumeViewButton];
    [self updateSliderWidth];
}

- (void)updateFullscreenFlagIfPossible
{
    if(self.fullscreenDelegate != nil)
    {
        _flags.isFullscreen = [self.fullscreenDelegate isMoviePlayerInFullscreen:self];
    }
}

- (void)setupVolumeView
{
    if(self.volumeView == nil)
    {
        AGMoviePlayerRouteView *volumeView = [[AGMoviePlayerRouteView alloc] init];
        volumeView.backgroundColor = TARGET_IPHONE_SIMULATOR && 0 ? [UIColor blueColor] : [UIColor clearColor];
        volumeView.showsVolumeSlider = NO;
        volumeView.showsRouteButton = YES;
        [volumeView sizeToFit];
        [self.volumeViewContainer insertSubview:volumeView belowSubview:self.buttonToggleFullscreen];
        self.volumeView = volumeView;
        
        self.volumeView.center = [self centerPositionForVolumeViewIfAirplayAvailable:[self.volumeView isOptionalRoutesAvailable]];
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            self.volumeView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin
            | UIViewAutoresizingFlexibleTopMargin
            | UIViewAutoresizingFlexibleBottomMargin
            | UIViewAutoresizingFlexibleLeftMargin;
        }
        else
        {
            self.volumeView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin
            | UIViewAutoresizingFlexibleTopMargin
            | UIViewAutoresizingFlexibleBottomMargin;
        }
    }
}

- (CGPoint)centerPositionForVolumeViewIfAirplayAvailable:(BOOL)avilable
{
    CGPoint p;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        p.x = CGRectGetMidX(self.volumeViewContainer.bounds);
        p.y = CGRectGetMidY(self.volumeViewContainer.bounds);
    }
    else
    {
        p.x = CGRectGetMinX(self.buttonToggleFullscreen.frame) - 16;
        p.y = CGRectGetMidY(self.volumeViewContainer.bounds);
    }
    
    if(!avilable)
    {
        p.y = -10000;
    }
    
    return p;
}

- (void)setupRecognizers
{
    UIView *bloatView = [[UIView alloc] initWithFrame:self.view.bounds];
    bloatView.backgroundColor = [UIColor clearColor];
    bloatView.opaque = NO;
    bloatView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backgroundDoubleTap:)];
    doubleTap.numberOfTapsRequired = 2;
    [bloatView addGestureRecognizer:doubleTap];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backgroundSingleTap:)];
    singleTap.numberOfTapsRequired = 1;
    [singleTap requireGestureRecognizerToFail:doubleTap];
    [bloatView addGestureRecognizer:singleTap];
    
    [self.view insertSubview:bloatView aboveSubview:self.moviePlayer.view];
}

- (void)toggleDisplayOfControls
{
    BOOL setHidden = _flags.controlsIsVisible;
    [self setControlsHidden:setHidden animated:YES];
}

- (void)setControlsHidden:(BOOL)hidden animated:(BOOL)animated
{
    [UIView animateWithDuration:animated ? 0.2 : 0.0 animations:^{
        _flags.controlsIsVisible = !hidden;
        
        [self.controlsContainers enumerateObjectsUsingBlock:^(UIView *container, NSUInteger idx, BOOL *stop) {
            container.alpha = hidden ? 0.0 : 1.0;
        }];
    }];
}

- (IBAction)startTimer:(id)sender
{
    [self endTimer:sender];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0 / 20.0 target:self selector:@selector(timerFired:) userInfo:nil repeats:YES];
}

- (IBAction)endTimer:(id)sender
{
    [self.timer invalidate];
    self.timer = nil;
}

- (void)timerFired:(NSTimer *)timer
{
    [self updateControlsAnimated:YES];
}

- (MPMoviePlayerController *)createMoviePlayer
{
    MPMoviePlayerController *player = [[MPMoviePlayerController alloc] initWithContentURL:self.contentURL];
    player.controlStyle = MPMovieControlStyleNone;
    player.view.frame = self.view.bounds;
    player.view.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    player.view.userInteractionEnabled = NO;
    player.shouldAutoplay = NO;    
    return player;
}

+ (void)enableAudioInSilenceMode
{
    /*
     http://thenextweb.com/apple/2012/10/04/its-not-just-you-the-mute-toggle-of-your-iphone-works-different-on-ios-6/
     */
    
    NSError *setCategoryErr = nil;
    NSError *activationErr  = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&setCategoryErr];
    [[AVAudioSession sharedInstance] setActive:YES error:&activationErr];
    
    if(setCategoryErr || activationErr)
    {
        NSLog(@"%@ Error: Could not activate silence mode. %@ %@", self, setCategoryErr, activationErr);
    }
}

- (void)layoutSubviews
{
    self.moviePlayer.view.frame = self.view.bounds;
}

- (NSString *)formatDuration:(NSTimeInterval)duration negative:(BOOL)negative
{
    BOOL isNan = duration != duration;
    
    if(duration < 0 || duration > 60*60*60*60*60 || isNan)
    {
        return @"00:00:00";
    }
    BOOL isOverOneHour = duration > 60 * 60;
    
    if(isOverOneHour)
    {
        NSString *string = [NSString stringWithFormat:@"%@%li:%02li:%02li",
                            negative ? @"-" : @"",
                            lround(floor(duration / 3600.)) % 100,
                            lround(floor(duration / 60.)) % 60,
                            lround(floor(duration)) % 60];
        return string;
    }
    else
    {
        NSString *string = [NSString stringWithFormat:@"%@%li:%02li",
                            negative ? @"-" : @"",
                            lround(floor(duration / 60.)) % 60,
                            lround(floor(duration)) % 60];
        return string;
    }
}

- (void)setupObservers
{
    if(self.moviePlayer == nil && self.view != nil && self.volumeView != nil)
    {
        [NSException raise:NSInternalInconsistencyException format:@"instance variables should exist when setting up observers"];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(movieFinishedCallback:) name:MPMoviePlayerPlaybackDidFinishNotification object:self.moviePlayer];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(movieChangedLoadState:) name:MPMoviePlayerLoadStateDidChangeNotification object:self.moviePlayer];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(movieChangedPlaybackState:) name:MPMoviePlayerPlaybackStateDidChangeNotification object:self.moviePlayer];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(movieDurationAvailable:) name:MPMovieDurationAvailableNotification object:self.moviePlayer];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(metaDataUpdated:) name:MPMoviePlayerTimedMetadataUpdatedNotification object:self.moviePlayer];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(airplayStateChanged:) name:MPMoviePlayerIsAirPlayVideoActiveDidChangeNotification object:self.moviePlayer];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewIsVisible:) name:AGMoviePlayerViewVisibleNotification object:self.view];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewIsNotVisible:) name:AGMoviePlayerViewNotVisibleNotification object:self.view];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(optionalRoutesAvailable:) name:AGMoviePlayerOptionalRoutesAvailableNotification object:self.volumeView];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (BOOL)isPlaying
{
    return self.moviePlayer.playbackState == MPMoviePlaybackStatePlaying;
}

- (CGFloat)sliderRectMaxX
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        BOOL optionalRoutesAvailable = [self.volumeView isOptionalRoutesAvailable];
        
        if(optionalRoutesAvailable)
        {
            return CGRectGetMinX(self.buttonToggleFullscreen.frame) - 36;
        }
        else
        {
            return CGRectGetMinX(self.buttonToggleFullscreen.frame) + 7;
        }
        
    }
    else
    {
        return self.slider.superview.bounds.size.width - 3;
    }
}

- (void)updateSliderWidth
{
    CGFloat sliderRectMaxX = [self sliderRectMaxX];
    CGRect newFrame = self.slider.frame;
    newFrame.size.width = sliderRectMaxX - CGRectGetMinX(self.slider.frame);
    if(!CGRectEqualToRect(newFrame, self.slider.frame))
    {
        self.slider.frame = newFrame;
    }
}

- (void)updateVolumeViewButton
{
    BOOL optionalRoutesAvailable = [self.volumeView isOptionalRoutesAvailable];
    CGPoint newCenter = [self centerPositionForVolumeViewIfAirplayAvailable:optionalRoutesAvailable];
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        CGRect containerFrame = self.volumeViewContainer.frame;
        containerFrame.origin.y = optionalRoutesAvailable ? 10 : -10000;
        self.volumeViewContainer.frame = containerFrame;
    }
    
    if(!CGPointEqualToPoint(newCenter, self.volumeView.center))
    {
        self.volumeView.center = newCenter;
    }
}

- (void)updateControlsAnimated:(BOOL)animated
{    
    if(!_flags.isScrubbing)
    {
        self.slider.value = self.moviePlayer.currentPlaybackTime;
    }
    
    self.slider.minimumValue = 0.0;
    self.slider.maximumValue = self.moviePlayer.duration;
    self.slider.progressValue = self.moviePlayer.playableDuration;
    
    self.activityIndicator.hidden = _flags.movieDurationAvailable;
    self.slider.hidden = !_flags.movieDurationAvailable;
    self.buttonPlay.hidden = !_flags.movieDurationAvailable || (self.moviePlayer.playbackState == MPMoviePlaybackStatePlaying);
    self.buttonPause.hidden = !_flags.movieDurationAvailable || (self.moviePlayer.playbackState != MPMoviePlaybackStatePlaying);
    
    if(_flags.movieDurationAvailable)
    {
        NSString *timeSpentText = [self formatDuration:self.slider.value negative:NO];
        NSString *durationText = [self formatDuration:self.slider.maximumValue negative:NO];
        self.labelTimeInfo.text = [NSString stringWithFormat:@"%@ / %@", timeSpentText, durationText];
        self.labelTimeInfo.textAlignment = NSTextAlignmentRight;
        CGRect frame = self.labelTimeInfo.frame;
        frame.origin.x = 24;
        self.labelTimeInfo.frame = frame;
    }
    else
    {
        self.labelTimeInfo.text = @"Laster...";
        self.labelTimeInfo.textAlignment = NSTextAlignmentLeft;
        CGRect frame = self.labelTimeInfo.frame;
        frame.origin.x = self.activityIndicator.frame.origin.x + self.activityIndicator.frame.size.width + 10;
        self.labelTimeInfo.frame = frame;
    }
    
    
    self.buttonToggleFullscreen.selected = _flags.isFullscreen;
    
    [self updateVolumeViewButton];
    [self updateSliderWidth];
}

#pragma mark - IBActions

- (IBAction)playPauseButtonTapped:(id)sender
{
    if([self isPlaying])
    {
        [self.moviePlayer pause];
    }
    else
    {
        [self.moviePlayer play];
    }
}

- (IBAction)sliderChangeDidStart:(AGSliderWithProgressBar *)slider
{
    _flags.isScrubbing = YES;
    _flags.wasPlayingBeforeScrubbing = [self isPlaying];
    [self.moviePlayer pause];
}

- (IBAction)sliderChanged:(AGSliderWithProgressBar *)slider
{
    if(!_flags.isScrubbing)
    {
        [self sliderChangeDidStart:slider];
    }
    
    self.moviePlayer.currentPlaybackTime = slider.value;
}

- (IBAction)sliderChangeDidEnd:(AGSliderWithProgressBar *)slider
{
    _flags.isScrubbing = NO;
    
    if(_flags.wasPlayingBeforeScrubbing)
    {
        
        [self.moviePlayer play];
    }
}

- (IBAction)toggleFullscreenButtonTapped:(id)sender
{
    if(!_flags.isFullscreen)
    {
        [self.fullscreenDelegate goFullscreenButtonTappedInMoviePlayer:self onDone:^{
            _flags.isFullscreen = YES;
            [self updateControlsAnimated:YES];
        }];
    }
    else
    {
        [self.fullscreenDelegate doneButtonTappedInMoviePlayer:self onDone:^{
            _flags.isFullscreen = NO;
            [self updateControlsAnimated:YES];
        }];
    }
}

- (void)backgroundSingleTap:(UITapGestureRecognizer *)recognizer
{
    [self toggleDisplayOfControls];
}

- (void)backgroundDoubleTap:(UITapGestureRecognizer *)recognizer
{
    [UIView animateWithDuration:0.14 animations:^{
        MPMovieScalingMode mode = self.moviePlayer.scalingMode == MPMovieScalingModeAspectFit ? MPMovieScalingModeAspectFill : MPMovieScalingModeAspectFit;
        self.moviePlayer.scalingMode = mode;
    }];
}

#pragma mark - Notifications 

- (void)movieChangedLoadState:(NSNotification *)notification
{
    [self updateControlsAnimated:YES];
}

- (void)movieFinishedCallback:(NSNotification *)notification
{
    [self updateControlsAnimated:YES];
}

- (void)movieChangedPlaybackState:(NSNotification *)notification
{
    if([self isPlaying])
    {
        [self startTimer:nil];
    }
    else
    {
        [self endTimer:nil];
    }
    
    [self updateControlsAnimated:YES];
}

- (void)movieDurationAvailable:(NSNotification *)notification
{
    _flags.movieDurationAvailable = YES;
    [self updateControlsAnimated:YES];
}

- (void)metaDataUpdated:(NSNotification *)notification
{
    
}

- (void)airplayStateChanged:(NSNotification *)notification
{
    [self updateControlsAnimated:YES];
}

- (void)viewIsVisible:(NSNotification *)notification
{
    [self updateControlsAnimated:YES];
}

- (void)viewIsNotVisible:(NSNotification *)notification
{
    if([self isPlaying])
    {
        [self.moviePlayer pause];
    }
}

- (void)optionalRoutesAvailable:(NSNotification *)notification
{
    [self updateVolumeViewButton];
    [self updateSliderWidth];
}

#pragma mark - Events from outside

- (void)isNowFullscreen
{
    _flags.isFullscreen = YES;
    [self updateControlsAnimated:YES];
}

- (void)isNowEmbedded
{
    _flags.isFullscreen = NO;
    [self updateControlsAnimated:YES];
}

@end