//
//  AGSliderWithProgressBar.h
//  AGSliderWithProgress
//
//  Created by Håvard Fossli on 30.01.13.
//  Copyright 2013 Håvard Fossli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface AGSliderWithProgressBar : UIControl {
@private
    struct {
        BOOL isTracking;
    } _flags;
}

@property (nonatomic, assign) CGFloat minimumValue;
@property (nonatomic, assign) CGFloat maximumValue;
@property (nonatomic, assign) CGFloat value;
@property (nonatomic, assign) CGFloat progressValue; 

@end
