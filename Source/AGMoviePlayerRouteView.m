//
//  AGMoviePlayerVolumeView.m
//  AGMoviePlayerView
//
//  Created by Håvard Fossli on 31.01.13.
//  Copyright 2013 Håvard Fossli. All rights reserved.
//

#import "AGMoviePlayerRouteView.h"
#import <QuartzCore/QuartzCore.h>

NSString * const AGMoviePlayerOptionalRoutesAvailableNotification = @"AGMoviePlayerOptionalRoutesAvailableNotification";
NSString * const AGMoviePlayerOptionalRoutesAvailabilityKey = @"AGMoviePlayerOptionalRoutesAvailabilityKey";

@interface AGMoviePlayerRouteView ()

@property (nonatomic, strong) UIButton *routeButton;

@end


@implementation AGMoviePlayerRouteView

#pragma mark - Construct and destruct

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {
        [self setup];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        [self setup];
    }
    return self;    
}

- (void)setup
{
    self.showsRouteButton = YES;
    self.showsVolumeSlider = NO;
    
    for (UIView *view in self.subviews) {
        if ([view isKindOfClass:[UIButton class]]) {
            self.routeButton = (UIButton *)view;
            [view addObserver:self forKeyPath:@"alpha" options:NSKeyValueObservingOptionNew context:nil];
        }
    }
}

- (CGRect)routeButtonRectForBounds:(CGRect)bounds
{
    return bounds;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if([keyPath isEqualToString:@"alpha"])
    {
        NSDictionary *userInfo = @{AGMoviePlayerOptionalRoutesAvailabilityKey : @([self isOptionalRoutesAvailable])};
        [[NSNotificationCenter defaultCenter] postNotificationName:AGMoviePlayerOptionalRoutesAvailableNotification object:self userInfo:userInfo];
    }
}

- (BOOL)isOptionalRoutesAvailable
{
    return self.routeButton.alpha > 0.0;
}

- (void)dealloc
{
    [self.routeButton removeObserver:self forKeyPath:@"alpha"];
}

@end
