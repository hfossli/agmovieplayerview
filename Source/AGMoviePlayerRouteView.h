//
//  AGMoviePlayerVolumeView.h
//  AGMoviePlayerView
//
//  Created by Håvard Fossli on 31.01.13.
//  Copyright 2013 Håvard Fossli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

/**
 View needs to be on window in order to send out notifications
 */
extern NSString * const AGMoviePlayerOptionalRoutesAvailableNotification;
extern NSString * const AGMoviePlayerOptionalRoutesAvailabilityKey;

@interface AGMoviePlayerRouteView : MPVolumeView

- (BOOL)isOptionalRoutesAvailable;

@end
