//
//  AGMoviePlayerViewController.h
//  VG
//
//  Created by Håvard Fossli on 29.01.13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import <UIKit/UIKit.h>

@class AGMoviePlayerViewController;

@protocol AGMoviePlayerViewControllerFullscreenDelegate <NSObject>

- (void)doneButtonTappedInMoviePlayer:(AGMoviePlayerViewController *)player onDone:(void(^)(void))onDone;
- (void)goFullscreenButtonTappedInMoviePlayer:(AGMoviePlayerViewController *)player onDone:(void(^)(void))onDone;
- (BOOL)isMoviePlayerInFullscreen:(AGMoviePlayerViewController *)player;

@end

@interface AGMoviePlayerViewController : UIViewController {
@private
    struct {
        BOOL isScrubbing;
        BOOL wasPlayingBeforeScrubbing;
        BOOL controlsIsVisible;
        BOOL isFullscreen;
        BOOL displaysVolumeViewButton;
        BOOL movieDurationAvailable;
    } _flags;
}

- (id)initWithContentURL:(NSURL *)contentURL;

@property (nonatomic, strong, readonly) MPMoviePlayerController *moviePlayer; 
@property (nonatomic, weak) id <AGMoviePlayerViewControllerFullscreenDelegate> fullscreenDelegate;

- (void)isNowFullscreen; // you are responsible for telling
- (void)isNowEmbedded; // you are responsible for telling 

@end



/*
 
 #import "AGWindowView.h"
 
 @property (nonatomic, assign) UIView *placeHolder;
 
- (void)goFullscreenAnimated:(BOOL)animated
{
    if(!self.isFullscreen)
    {
        self.placeHolder = [self createPlaceHolderFromCurrentState];
        
        AGWindowView *windowView = [[AGWindowView alloc] initAndAddToKeyWindow];
        [windowView addSubviewAndFillBounds:player.view withSlideUpAnimationOnDone:nil];
        [windowView forceToInterfaceOrientation:orientation];
    }
}

- (void)goBackAnimated:(BOOL)animated
{
    if(self.isFullscreen)
    { 
    }
}

*/