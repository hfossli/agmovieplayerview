//
//  VGStretchableButton.m
//  VG
//
//  Created by Håvard Fossli on 19.12.11.
//  Copyright 2011 Agens. All rights reserved.
//

#import "AGStretchableButton.h"

@interface AGStretchableButton ()

- (void)setup;

@end


@implementation AGStretchableButton

#pragma mark - Construct and destruct

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self) {
        [self setup];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        [self setup];
    }
    return self;    
}

- (void)setup { 
    
    UIImage *normalBackground = [self backgroundImageForState:UIControlStateNormal];
    if(normalBackground != nil) {
        normalBackground = [normalBackground stretchableImageWithLeftCapWidth:floor(normalBackground.size.width / 2) topCapHeight:floor(normalBackground.size.height/2)];
        [self setBackgroundImage:normalBackground forState:UIControlStateNormal];
    }
    
    UIImage *highlightedBackground = [self backgroundImageForState:UIControlStateHighlighted];
    if(highlightedBackground != nil && highlightedBackground != normalBackground) {
        UIImage *highlightedBackgroundStretched = [highlightedBackground stretchableImageWithLeftCapWidth:floor(normalBackground.size.width / 2) topCapHeight:floor(normalBackground.size.height/2)];
        [self setBackgroundImage:highlightedBackgroundStretched forState:UIControlStateHighlighted];
    }
    
    UIImage *selectedBackground = [self backgroundImageForState:UIControlStateSelected];
    if(selectedBackground != nil && selectedBackground != normalBackground) {
        UIImage *selectedBackgroundStretched = [selectedBackground stretchableImageWithLeftCapWidth:floor(normalBackground.size.width / 2) topCapHeight:floor(normalBackground.size.height/2)];
        [self setBackgroundImage:selectedBackgroundStretched forState:UIControlStateSelected];
    }
    
}

@end
