//
//  AGViewController.h
//  AGMoviePlayerView
//
//  Created by Håvard Fossli on 29.01.13.
//  Copyright (c) 2013 Håvard Fossli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AGMoviePlayerViewController.h"

@interface AGViewController : UIViewController <AGMoviePlayerViewControllerFullscreenDelegate>

@end
