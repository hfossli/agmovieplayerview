//
//  AGViewController.m
//  AGMoviePlayerView
//
//  Created by Håvard Fossli on 29.01.13.
//  Copyright (c) 2013 Håvard Fossli. All rights reserved.
//

#import "AGViewController.h"
#import "AGWindowView.h"

@interface AGViewController ()

@property (nonatomic, strong) AGMoviePlayerViewController *player;
@property (nonatomic, strong) AGWindowView *windowView;
@property (nonatomic, strong) UIView *placeHolder;

@end

@implementation AGViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    NSURL *videoURL = [NSURL URLWithString:@"http://tablet.vg.no/ipad/play.php?id=60643"];
    self.player = [[AGMoviePlayerViewController alloc] initWithContentURL:videoURL];
    self.player.fullscreenDelegate = self;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        self.player.view.frame = CGRectMake(5, 100, 310, 232);
    }
    else
    {
        self.player.view.frame = CGRectMake(20, 100, 400, 300);
    }
    
    [self.view addSubview:self.player.view];
    
    
}

- (UIView *)createPlaceHolderFromCurrentStateOfView:(UIView *)view
{
    UIView *placeHolder = [[UIView alloc] initWithFrame:view.frame];
    placeHolder.backgroundColor = [UIColor clearColor];
    [self.view.superview insertSubview:placeHolder aboveSubview:view];
    return placeHolder;
}

- (void)replacePlaceHolder:(UIView *)placeholder withView:(UIView *)view
{
    view.frame = placeholder.frame;
    [placeholder.superview insertSubview:view aboveSubview:placeholder];
    [placeholder removeFromSuperview];
}

- (void)doneButtonTappedInMoviePlayer:(AGMoviePlayerViewController *)player onDone:(void (^)(void))onDone
{
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait animated:YES];
    
    [UIView animateWithDuration:0.3 animations:^{
        CGRect newFrame = [self.placeHolder convertRect:self.placeHolder.bounds toView:self.windowView];
        self.player.view.frame = newFrame;
    } completion:^(BOOL finished) {
        [self replacePlaceHolder:self.placeHolder withView:self.player.view];
        [self.windowView removeFromSuperview];
        self.windowView = nil;
        onDone();
    }];
}

- (void)goFullscreenButtonTappedInMoviePlayer:(AGMoviePlayerViewController *)player onDone:(void (^)(void))onDone
{
    self.placeHolder = [self createPlaceHolderFromCurrentStateOfView:self.player.view];
    
    self.windowView = [[AGWindowView alloc] initAndAddToKeyWindow];
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationLandscapeLeft animated:YES];
    self.windowView.supportedInterfaceOrientations = UIInterfaceOrientationMaskLandscapeLeft;
    [self.windowView addSubViewAndKeepSamePosition:self.player.view];
    self.windowView.clipsToBounds = YES;
    self.player.view.clipsToBounds = YES;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.player.view.frame = self.windowView.bounds;
    } completion:^(BOOL finished) {
        onDone();
    }];
}

- (BOOL)isMoviePlayerInFullscreen:(AGMoviePlayerViewController *)player
{
    return self.windowView != nil;
}

- (IBAction)remove:(id)sender
{
    [self.player.view removeFromSuperview];
    self.player = nil;
}

- (IBAction)remove2:(id)sender
{
    [self goFullscreenButtonTappedInMoviePlayer:self.player onDone:^{}];
    
    double delayInSeconds = 0.5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self.windowView removeFromSuperview];
        self.windowView.controller = nil;
        self.windowView = nil;
        self.player = nil;
    });
}


@end
