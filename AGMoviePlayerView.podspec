Pod::Spec.new do |s|
  s.name = 'AGMoviePlayerView'
  s.version = "0.2.5"
  s.summary = 'Custom skinned movie player used in VG+'
  s.homepage = 'https://bitbucket.org/hfossli/agmovieplayerview/overview'
  s.author = { 'Håvard Fossli' => 'hfossli@agens.no' }
  s.source       = {
      :git => 'https://hfossli@bitbucket.org/hfossli/agmovieplayerview.git',
      :tag => s.version.to_s
      }
  s.source_files = 'Source/*.{h,m,pch}'
  s.exclude_files = 'Source/*Test.{h,m}'
  s.frameworks   = 'UIKit', 'MediaPlayer', 'Foundation', 'AVFoundation'
  s.platform = :ios, '8.0'
  s.requires_arc = true
  s.resources = 'Source/*.{xib}' , 'Graphics/*'
end
